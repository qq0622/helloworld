package com.neusoft.hello;

import java.net.StandardSocketOptions;

public class name {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		/*
		 * double a=10.2; System.out.println(a);
		 */
		/*
		 * String name="小明";//给name赋值 String age="15";//给age赋值 String
		 * sorcn="87.5456456";//给sorcn赋值 char af='1'; String a="ssda";
		 * System.out.println("姓名"+name+"年龄"+age+"成绩"+sorcn+af);//打印姓名小明年龄15成绩87
		 * .5456456 System.out.println(a); char chVal1='B';
		 * System.out.println(chVal1+2);
		 */
		method0();
		// method1();
		// method2();
		 method3();
		// method4();
		 method5();
		// method6();
		// method7();
		// method8();
		// method9();
		// method10();
		// method11();
//		method12();
//		method13();
		method14();
		method15();
		/*
		 * int i = 1; int j = 1; int k = 1; int l = 1; System.out.println(i);
		 * System.out.println(i++); System.out.println(i); // 后加
		 * System.out.println(j); System.out.println(j--);
		 * System.out.println(j); // 后减 System.out.println(k);
		 * System.out.println(++k); System.out.println(k); // 先加
		 * System.out.println(l); System.out.println(--l);
		 * System.out.println(l);
		 */
	}

	// 先减
	private static void method1() {
		int a = 1;
		int b = 2;
		int c = 3;
		int d = 4;
		System.out.println("a=" + a + "b=" + b + "c=" + c + "d=" + d);
		boolean test = a++ < b++ && b++ < c++ || !(c++ > d++);
		System.out.println("a=" + a + "b=" + b + "c=" + c + "d=" + d);
		System.out.println("结果" + test);
	}

	private static void method2() {
		int a = 1;
		int b = 3;
		int c = 2;
		int d = 4;
		System.out.println("a=" + a + "b=" + b + "c=" + c + "d=" + d);
		boolean test = a++ < b++ && b++ > c++ || !(c++ > d++);
		System.out.println("a=" + a + "b=" + b + "c=" + c + "d=" + d);
		System.out.println("结果" + test);
	}
	private static void method0() {// 课后作业1
		int a = 2; // 给a赋值1
		int b = 0; // 给b赋值2
		a^=b;
		b^=a;
		a^=b;
		System.out.println("a=" + a + ";b=" + b); // a和b互换
	}
	private static void method3() {// 课后作业1
		int a = 1; // 给a赋值1
		int b = 0; // 给b赋值2
		int c = a; // 把a的值赋给c
		a = b; // 把b的值赋给a
		b = c; // 把c的值赋给b
		System.out.println("a=" + a + ";b=" + b); // a和b互换
	}

	private static void method4() {// 课后作业2
		int a = 234; // 给a赋值234
		int b = a / 100; // 234除100得出百位b的值
		int c = a % 100 / 10; // 234除100取余数，在除以10 的到十位c的值
		int d = a % 100 % 10; // 234除100取余数在除以10取余数得到个位d的值
		System.out.println(b + "+" + c + "+" + d); // 输出 b，c，d三位数
		System.out.println(b + d + d); // 输出三位数的和
	}

	private static void method5() {// 课后作业3
		double a = 69.8;// 输入华氏温度
		double b = (a - 32) * 5 / 9; // 华氏温度转换摄氏公式
		double c = 21.0;// 输入摄氏温度
		double d = c * 9 / 5 + 32;// 摄氏温度转换华氏温度公式
		System.out.println(b + "摄氏度," + d + "华氏温度");// 输出摄氏温度和华氏温度
	}

	private static void method6() {// 课后作业4
		char a = 'd';// 给a赋值小写字母d
		char b = (char) (a - 32);// 小写字母转换大写字母公式
		char f = 'G';// 给f赋值小写字母d
		char h = (char) (f + 32);// 小写字母转换大写字母公式
		System.out.println(b + "和" + h); // 输出大写字母
	}

	private static void method7() {// 练习1
		System.out.println(3 + 4 + "hello！");// 打印出7hello！
		System.out.println("hello！" + 3 + 4);// 打印hello！34
	}

	private static void method8() {// 练习2
		double a = 3;
		double b = Math.pow(a, 3);// 计算a的5次方赋值给b
		System.out.println(b);// 输出a的5次方
		double c = Math.sqrt(a);// 计算a的平方根赋值给c
		System.out.println(c);// 输出c
		double d = Math.random();// 计算随机数返回一个0~1的随机数
		System.out.println(d);// 输出随机数d
		double e = Math.sin(1.57);// 计算1.57的sin值赋值给e
		System.out.println(e);// 输出e

	}

	private static void method9() {// 练习3
		System.out.println("5是否大于4.0:" + (5 > 4.0));
		System.out.println("5和5.0是否相等：" + (5 == 5.0));
		System.out.println("97和'a'是否相等:" + (97 == 'a'));
		System.out.println("true和false是否相等:" + (true == false));
	}

	private static void method10() {// 练习4
		int a = 5;
		int b = 10;// 定义变量a和b
		if (a > 4 | b++ > 10)
			;
		System.out.println("a的值是:" + a + "b的值是:" + b);
	}

	private static void method11() {// 练习5
		int a = 5;
		int b = 10;
		if (a > 4 || b > 10)
			;
		System.out.println("a的值是:" + a + "b的值是:" + b);
	}

	private static void method12() {//练习6
		String str = 5 > 3 ? "5大于3" : "5小于3";
		System.out.println(str);
	}

	private static void method13() {//练习7
		String str2 = null;
		if (5 > 2) {
			str2 = "5大于2";
		} else if (5 == 2) {
			str2 = "5等于2";
		} else {
			str2 = "5小于2";
		}
		System.out.println(str2);
	}

	private static void method14() {//练习8
		int a = 11;
		int b = 12;
		System.out.println(a > b ? "a大于b" : (a < b ? "a小于b" : "a等于b"));
	}
	private static void method15() {//练习9
		int a=5;
	
		System.out.println("输出"+a);
		}
}
 